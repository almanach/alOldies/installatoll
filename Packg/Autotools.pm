package Packg::Autotools;

use strict;
use File::Basename;
use base qw/Packg/;

# this pkg uses autotools


sub autoreconf
{
    my $self = shift;

    # DyALog has incompatibility with autoconf >=2.60
    # _AC_SRCPATHS replaced by _AC_SRCDIRS in configure.ac
    if($self->{NAME} eq 'DyALog' && $Packg::package_list{autoconf}->version >= 2.60) {
	my $dir = fileparse($Packg::config->prefix."/src/DyAlog/");
	my $old = $dir."configure.ac";
	my $new = $dir."configure.ac.new";
	open(OLD, "<", $old) or die "can't open $old: $!";
	open(NEW, ">", $new) or die "can't open $new: $!";
	while (<OLD>) {
	    if (/^_AC_SRCPATHS/) {
		print NEW "_AC_SRCDIRS([\".\"])";
	    }
	    else {
		print NEW $_;
	    }
	}
	close(OLD)                or die "can't close $old: $!";
	close(NEW)                or die "can't close $new: $!";
	rename($old, "$old.orig") or die "can't rename $old to $old.orig: $!";
	rename($new, $old)        or die "can't rename $new to $old: $!";
    }

    # export automake env variable
    $ENV{AUTOMAKE} = "automake-dyalog" if (exists $self->{DEPEND}->{DyALog});

     $self->cmd( msg => 'autoreconf', 
 		cmd => 'autoreconf', 
 		options => ["-i"]);

    delete $ENV{AUTOMAKE};
}

sub configure
{
    my $self = shift;
    my $prefix = $Packg::config->prefix;
    my @options = ("--prefix=$prefix");
    push(@options,@{$self->{CONFIGURE}}) if (exists $self->{CONFIGURE});
    push(@options,'--enable-rawscripts') if ($self->{NAME} eq 'lefff' && $Packg::config->linux32);
    $self->cmd( msg => "configure @options", 
		cmd => "./configure",
		options => [ @options ]);
}

############################################
# Purpose    : Generate Makefile

sub create_make {
    my $self = shift;
    $self->autoreconf() if ($Packg::config->gforge_user && $self->{SVN});
    
    # change .pl files date (DyALog being bootstrapped)
    if ($self->{NAME} eq 'DyALog' && $Packg::config->gforge_user ne '') {
	my $time = time() - 120;
	my @pl_files = glob ("Compiler/*.pl");
	utime($time,$time,@pl_files);
    }
    
    $self->configure();
}

sub test {
    my $self = shift;
    $self->cmd( msg     => "make check", 
		cmd     => "make",
		options => [ "check" ]);
}

1;
