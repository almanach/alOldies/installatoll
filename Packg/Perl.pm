package Packg::Perl;

use strict;
use base qw/Packg/;

# this pkg is a perl pkg

############################################
# Purpose    : Get version number

sub version {
    my $self = shift;

    my $mod = @{$self->{VERSION}}[1];

    return 0 unless (-e $Packg::LENV{PERLDIRV}."/$mod.pm"); # tmp
    
    require "$mod.pm";
    return "$mod"->VERSION;
}

############################################
# Purpose    : Generate Makefile

sub create_make {
    my $self = shift;

    # create Makefile
    $self->cmd( msg => "configure (with Perl)",
		cmd => "perl",
		options => ['Makefile.PL',
			    "PREFIX=$Packg::LENV{PREFIX}", 
			    "INSTALLSITEBIN=$Packg::LENV{BINDIR}",
			    "INSTALLSITESCRIPT=$Packg::LENV{BINDIR}",
			    "INSTALLSITEMAN1DIR=$Packg::LENV{MANDIR}/man1",
			    "INSTALLSITEMAN3DIR=$Packg::LENV{MANDIR}/man3",
			    ]
		); 
}

sub test {
    my $self = shift;
    $self->cmd( msg     => "make manifest", 
		cmd     => "make",
		options => [ "manifest" ]);
    $self->cmd( msg     => "make test", 
		cmd     => "make",
		options => [ "test" ]);
}

1;
