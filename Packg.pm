package Packg;
use strict;
use IPC::Run qw( run timeout ) ;
use File::Basename;


our %package_list;
our $config;
our $revconfig;
our %LENV;


# TODO changer verbose
sub verbose {
    main::verbose (@_);
}

sub ini
{
    ($config, $revconfig) = @_;
    %LENV   = %{$_[2]};
}

############################################
# Purpose    : create new object package

sub new
{
    my ($class, $fields) = @_;
    my $self = bless $fields, $class;

    # store revision number if present in rev.conf
    if (!$self->{NONLOCALDEP}){
	my $name = $self->{NAME};
	$self->{REVISION} = $revconfig->$name if ($revconfig->$name);
    }

    return $self;
}

############################################
# Purpose    : create and register package in %package_list
# Parameters : %package

sub register
{
    my($fields) = @_;
    my $class = 'Packg';
    $class .= '::'.${%$fields}{TYPE} if (${%$fields}{TYPE});

    # create and register
    $package_list{${%$fields}{NAME}} = $class->new($fields);
}

############################################
# Purpose    : execute command, print output to log file
# Parameters : $self, %info which contains msg (message to be printed 
#              at execution), cmd and options

sub cmd 
{
    my ($self, %info) = @_;

    verbose "\t$info{msg}" if (defined  $info{msg});
    my $cmd = $info{cmd};
    my $options = $info{options} || [];
    my @linux32 = ();
    push(@linux32,'linux32') if ($config->linux32);

    my $out = '';

    # TODO passer LOG en arg
    my $ok = run [@linux32,$cmd,@$options], '>', \$out,'2>',\*main::LOG;
    print( main::LOG $out);

    if (!$ok) {
	return 0 if ($cmd eq 'pkg-config' || $cmd eq 'dyalog-config'); # don't exit on this error
	my $error = $config->prefix."/src/error_".$self->{NAME};
	open FILE, ">>$error" or die "$!, stopped"; #keep trace of error for later install
	close FILE;
	my $log = $config->log;
	die "$self->{NAME} not installed, check $log for details, stopped";
    }

    return $out;
}

############################################
# Purpose    : fetch tar.gz from ftp
# Returns    : 1 if new revision was retrieved

sub fetch_from_ftp {
    my $self = shift;

    my $url;
    if ($self->{FTP}){
	$url = $self->{FTP};
    }
    else {
	die "$!\ncannot fetch from FTP: no FTP address provided in main file, stopped";
    }
    my $local = basename($url);
    my $localdir = basename($local,'.tar.gz');

    # fetch tarball unless it is already there
    if (-e $local && !$config->force) {
	chdir $self->{NAME};
	return 0;
    }

    # remove directory (otherwise there is a pb with rename, and if previous 
    # version was fetched from svn, .svn/ needs to be deleted anyway)
    die "Please remove previous version of $self->{NAME}."
	if (-d "$self->{NAME}");
    

    $self->cmd( msg => "fetching online",
		cmd => "wget",
		# last option to be able to download from gforge
		options => [$url, "--no-check-certificate"] 
		);   

    $self->cmd( msg => "untar",
		cmd => "tar",
		options => ["xzvf","$local"]
		);
    
    # just keep the package name (without the version number)
    if ($localdir ne $self->{NAME}) {
	rename($localdir, $self->{NAME}) or die "can't rename\n";
    }
    chdir $self->{NAME};
    return 1;
}

############################################
# Purpose    : checkout or update sources from Subversion repository
# Returns    : 1 if new revision was retrieved

sub fetch_from_svn {
    my $self = shift;
    my $dev = $config->gforge_user;

    # revision: number specified by the developper or last revision (HEAD)
    my $rev = '-r';
    if (exists $self->{REVISION}) {
	$rev .= "$self->{REVISION}";
    }
    else {
	$rev .= 'HEAD';
    }

    # if there is no working copy, then checkout
    if (!-d "$self->{NAME}/.svn/") {

	# remove directory if previous version was fetched from ftp
	# (if pkg directory exists and there is no .svn/ directory in it,
        # then it means the previous version was fetched from ftp)
	die "Please remove previous version of $self->{NAME} (not a working copy)."
	    if (-d $self->{NAME});

	if ($dev ne 'anonymous') {

	    if ( $self->cmd( msg => "fetching from svn+ssh",
			     cmd => 'svn',
			     options => [qw/co /, $rev, "svn+ssh://".$dev."@".$self->{SVN}, $self->{NAME}] ) 
		 eq '0' ) {
		
		print STDERR "cannot fetch from svn: check gforge name or ssh key\n";
	    }

	}
	else {
	    $self->cmd( msg => "fetching from svn",
			cmd => 'svn',
			options => [qw/co /, $rev, "svn://".$self->{SVN}, $self->{NAME}] 
			);
	}

	chdir $self->{NAME};
	my $v = $self->cmd( cmd => 'svnversion',
			    options => ["."]
			    );
	chomp $v;
	$self->{V} = $v;
    }

    elsif (-d "$self->{NAME}/.svn/"){ 
	chdir $self->{NAME};	
	my $out = $self->cmd( msg => "updating from svn",
			      cmd => 'svn',
			      options => [qw/up ./, $rev]
			      );

	my $n = () = ($out =~ m/\n/g);

	my $v = $self->cmd( cmd => 'svnversion',
			    options => ["."]
			    );
	chomp $v;
	$self->{V} = $v;

	return 0 if ($n == 1 && !$config->force);
    }
    return 1;
}

############################################
# Purpose    : Download package sources
# Returns    : 1 if new version was retrieved

sub download
{
    my $self = shift;

    chdir $config->prefix."/src/";

    if ($config->gforge_user && $self->{SVN} 
	&& !(exists $self->{PRIVATE} && $config->gforge_user eq 'anonymous')
	){
	return $self->fetch_from_svn;
    }
    else {
	if (!$self->{FTP}) {
	    print "There is no release for this package.";
	    return 0;
	}
	return $self->fetch_from_ftp;
    }
}

############################################
# Purpose    : Update parserd.conf
# Comment    : update parserd.conf without overwriting info

sub update_conf 
{
    ## done in parserd
    my $self = shift;

    my $conffile = "$LENV{ETC}/parserd.conf";
    open(CONF,"<$conffile") or die "can't postprocess $conffile: $!, stopped";

    my $info;
    my $parsers;
    while(<CONF>) {
	#$content .= "$_" unless (/^\#\# Local options/ || /^port/ || /^user/ || 
		#		 /^group/ || /^log_file/ || /pid_file/ || /^path/);

	unless (/^\#\# Local options/ || /^port/ || /^user/ || 
		/^group/ || /^log_file/ || /pid_file/ || /^path/) {
	    if (/^\# /) {
		$info .= "$_";
	    }
	    else{
		$parsers .= "$_";
	    }
	}
    }

    close(CONF);

    open(CONF,">$conffile") or die "can't postprocess $conffile: $!, stopped";
    my $port    = $config->port;
    my $user    = $config->user;
    my $group   = $config->group;

    print CONF <<EOF;
$info

## Local options
port = $port
user = $user
group = $group
log_file = $LENV{VAR}/log/parserd.log
pid_file = $LENV{VAR}/run/parserd.pid
path  = $LENV{BINDIR}
path  = /usr/local/bin
path  = /usr/ucb

$parsers
EOF
}

############################################
# Purpose    : Update dependencies of this pkg before updating it

sub update_depend 
{
  my $self = shift;

  foreach my $dep ( sort keys %{$self->{DEPEND}} ) {
      $package_list{$dep}->update();
  }
}

############################################
# Purpose    : Get version number
# Returns    : Version number

sub version
{
    my $self = shift;

    my $cmd = @{$self->{VERSION}}[1];
    my @options = split (/ /, @{$self->{VERSION}}[2]);

    return 0 if ((!-x "/usr/bin/$cmd") && (!-x $Packg::LENV{BINDIR}."/$cmd") ) ;
    my $version = $self->cmd( cmd     => $cmd,
			      options => [ @options ]);
    chomp $version;

    ($version) = ( $version =~ m/(\d[\.\d]*)/ ) if ($version =~ m/^\D/); # extract version number

    return $version if($version);
    return 0;
}

############################################
# Purpose    : Check if package is up to date
# Returns    : 1 if up to date, else 0

sub uptodate
{
    my $self = shift;

    return 0 if ($config->force && !$self->{NONLOCALDEP});

    # check version
    my $version;

    if (exists $self->{VERSION}) {

	$version = $self->version();
	return 0 if (!$version);
	my @newversion  = split /\./, ${$self->{VERSION}}[0];
	my @instversion = split /\./, $version;
	
	foreach my $num (@newversion) {
	    my $inst = shift @instversion;
	    return 0 if ($num > $inst);
	}
    }

    # check if a file is missing
    if (exists $self->{INSTALLED}) {
	my $installed = 1;
	foreach my $file (@{$self->{INSTALLED}}) {
	    if (!-e "/usr/$file") {
		$installed = 0;
		last;
	    }
	}
	unless ($installed) {
	    foreach my $file (@{$self->{INSTALLED}}) {
		return 0 if (!-e "$LENV{PREFIX}/$file");
	    }
	}
    }

    my $uptodate = "$self->{NAME} is up to date";
    $uptodate   .= " ($version)" if ($version);
    verbose $uptodate unless $config->dependencies;
    $self->{UPDATED} = 1;
    return 1;
}


############################################
# Purpose    : Postprocess after make install

sub postprocess 
{
    my $self = shift;

    while ( my ($process, $args) = each( %{$self->{POSTPROCESS}} ) ) {

	if    ($process eq 'bininstall') {
	    foreach my $binary (@$args) {
		$self->cmd( msg => "install $binary",
			    cmd => 'install',
			    options => ["$binary","$LENV{BINDIR}/$binary"]
			    );
	    }
	}
	elsif ($process eq 'pkgshareinstall') {
	    my $pkgsharedir = "$LENV{SHAREDIR}/$self->{NAME}";
	    mkdir $pkgsharedir;

	    foreach my $file (@$args) {
		$self->cmd( msg => "install $file",
			    cmd => 'install',
			    options => ["$file","$pkgsharedir/$file"]
			    );
	    }
	}
	elsif ($process eq 'cmd') {
	    $self->cmd(%$args);
	}
	elsif ($process eq 'update_conf') {
	    $self->update_conf();
	}
    }
}

############################################
# Purpose    : Install or update package

sub update 
{
    my $self = shift;

    foreach my $pkg (@{$config->skip_pkg}) {
	return if ($self->{NAME} eq $pkg);
    }

    return if ($self->{UPDATED} || ($self->{NONLOCALDEP} && $self->uptodate));

    $self->update_depend() unless ($config->skip_dep); # update dependencies first

    Packg::verbose "----------------\nHandling package $self->{NAME}\n----------------";
    # eventual error flag from previous install
    my $error = $config->prefix."/src/error_".$self->{NAME};

    if ($self->download() || -e $error || !-f "Makefile") {
	# different according to pkg type (Autotools, Perl...)
	$self->create_make(); 
    }

    if (-f "Makefile") {
	$self->cmd( msg => "make", cmd => 'make');
	## test alpage packages
	$self->test() if ($config->test && !$self->{NONLOCALDEP} && ($self->{NAME} ne "syntax"));
	$self->cmd( msg => "install", cmd => 'make', options => ['install']);
    }
    $self->postprocess if ($self->{POSTPROCESS});

    chdir "$config->prefix/";
    $self->{UPDATED} = 1;
    unlink $error;
    verbose "done";
}

############################################
# Purpose    : Create distribution

sub makedist
{
    my $self = shift;
    Packg::verbose "----------------\nHandling package $self->{NAME}\n----------------";    
    chdir $config->prefix."/src/".$self->{NAME};
    $self->cmd( msg => "make dist", cmd => 'make', options => ['distcheck']);
    verbose "done";
}

1;
