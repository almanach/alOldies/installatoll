#!/usr/bin/perl
use strict;
use AppConfig qw/:argcount :expand/;
use File::Basename;
use Packg;
use Packg::Perl;
use Packg::Autotools;

our $VERSION = '0.2';

# read configuration file / parse command line arguments
my $config = AppConfig->new(
                            "verbose!"          => {DEFAULT => 1},
			    "prefix=f"          => {DEFAULT => "$ENV{HOME}/exportbuild"},
			    "aclocal=f"         => {DEFAULT => "aclocal-1.9"},
			    "archive=f"         => {DEFAULT => "frmgchain"},
			    "package|pkg=s@ "   => {DEFAULT => []},
			    "skip_pkg=s@ "      => {DEFAULT => ['']},
			    "skipdep|skip_dep!"          => {DEFAULT => 0},
			    "vardir=f"          => {DEFAULT => ''},
			    "run!"              => {DEFAULT => 1},
			    "linux32!"          => {DEFAULT => 0},
			    "port|p=s"          => {DEFAULT => 9043},
			    "user|u=s"          => {DEFAULT => ''},
			    "group|g=s"         => {DEFAULT => ''},
			    "gforge_user|gf|svn=s"  => {DEFAULT => ''},
			    "makedist=s@ "      => {DEFAULT => []},
			    "force|f!"          => {DEFAULT => 0},
			    "dependencies|dep!" => {DEFAULT => 0},
			    "version|v!"        => {DEFAULT => 0},
			    "project!"          => {DEFAULT => 0},
			    "makedist!"         => {DEFAULT => 0},
			    "rev|r!"            => {DEFAULT => 0},
			    "info|i!"           => {DEFAULT => 0},
			    "log=s"             => {DEFAULT => 'installatoll.log'},
			    "test!"             => {DEFAULT => 0},
                           );

## which revision or version
my $revconfig = AppConfig->new(
			       "DyALog=s"               => {DEFAULT => ''},
			       "dyalog-sqlite=s"        => {DEFAULT => ''},
			       "dyalog-xml=s"           => {DEFAULT => ''},
			       "lexed=s"                => {DEFAULT => ''},
			       "sxpipe=s"            => {DEFAULT => ''},
			       "sxpipe-data=s"          => {DEFAULT => ''},
			       "Lingua-Matcher=s"       => {DEFAULT => ''},
			       "Lingua-Features=s"      => {DEFAULT => ''},
			       "Lingua-MAF=s"           => {DEFAULT => ''},
			       "Lingua-TagSet=s"        => {DEFAULT => ''},
			       "forest_utils=s"         => {DEFAULT => ''},
			       "parserd=s"              => {DEFAULT => ''},
			       "Dict-Lexed=s"           => {DEFAULT => ''},
			       "mgcomp=s"               => {DEFAULT => ''},
			       "mgtools=s"              => {DEFAULT => ''},
			       "frmg=s"                 => {DEFAULT => ''},
			       "biomg=s"                => {DEFAULT => ''},
			       "alexina-tools=s"        => {DEFAULT => ''},
			       "lefff=s"                => {DEFAULT => ''},
			       "lefff-frmg=s"           => {DEFAULT => ''},
			       "syntax=s"               => {DEFAULT => ''},
			       "tag_utils=s"               => {DEFAULT => ''},
		       );

my $conffile = 'installatoll.conf';
my $revconffile = 'rev.conf';
my @SAVEDARGS=@ARGV;

if (@ARGV && $ARGV[0] =~ m/^--config/) {
    shift @ARGV;
    $conffile = shift @ARGV;
    print STDERR "USING config file $conffile\n";
}

if (@ARGV && $ARGV[0] =~ m/^--revconfig/) {
    shift @ARGV;
    $revconffile = shift @ARGV;
    print STDERR "USING config file $revconffile\n";
}

if (-r $conffile) {
    $config->file("$conffile")
	|| die "can't open or process configuration file $conffile";
}

if (-r $revconffile) {
    $revconfig->file("$revconffile")
	|| die "can't open or process configuration file $revconffile";
}

$config->args();
$revconfig->args();

## check a few options
# test if 64b
if (!$config->linux32) {
    my $bit = `uname -m`;
    if ($bit =~ m/64/) {
       die "Please activate option linux32.\n";
    }
}
if ($config->user eq '') {
    my $user = `echo \$USER`;
    chomp $user;
    $config->set('user', $user);
}
elsif ($config->user eq 'root'){
    print "This script should be run locally, not as root...";
    exit;
}
if ($config->group eq '') {
    $config->set('group', (`groups` =~ m/^(\w+)/));
}

my $prefix      = $config->prefix;
my $src         = "$prefix/src";
my $vardir      = $config->vardir || $prefix;
my $log         = $config->log;
my $perlversion = sprintf("%vd",$^V);
my $atollftp    = "ftp://ftp.inria.fr/INRIA/Projects/Atoll/Eric.Clergerie";
my $atollsvn    = "scm.gforge.inria.fr/svn";
my %LENV;
my $setenv_script;
my $parserd_setenv_script;

initenv();

print STDERR "prefix=$prefix\n";


# open log file
open(LOG,">$log") or die "can't open $log: $!";
my $date = `date`;
my $info = `uname -a`;
chomp $date;
chomp $info;
verbose(<<EOF);
Running $0 @SAVEDARGS
$date
$info
----------------------------------
EOF

############################################
# Purpose    : print information for the user and write it in log file

sub verbose {
  return unless $config->verbose;
  print STDERR @_, "\n";
  print LOG @_,"\n";
}


# create directories
mkdir "$prefix";
mkdir "$vardir";
chdir "$vardir";

foreach my $dir (qw{var var/log var/run}) {
    mkdir $dir;
}
chdir "$prefix";

foreach my $dir (qw{share share/aclocal lib bin sbin etc etc/init.d}) {
    mkdir $dir;
}
mkdir $src;



#initialize global variables in module Packg
Packg::ini($config, $revconfig, \%LENV);

#define objects types (defines the way they will be compiled)
my $autotools = 'Autotools';
my $perl      = 'Perl';

my $lingwb    = 'ALPAGE Linguistic Workbench';
my $mgkit     = 'Metagrammar Toolkit';

# create and register ALPAGE packages:

# project DyALog
#################################################################

Packg::register( { NAME        => 'DyALog',
		   DESC        => 'a parser compiler and logic programming environment',
		   TYPE        => $autotools,
		   FTP         => "$atollftp/DyALog/DyALog-1.11.3.tar.gz",
		   SVN         => "$atollsvn/dyalog/DyALog/trunk",
		   CONFIGURE   => ["--includedir=/usr/lib"],
		   PROJECT     => 'dyalog',
		   DEPEND      => { 'gc6.6'  => 1 },
		   VERSION     => ["1.11.3", "dyalog-config", "--version"],
	       });

Packg::register( { NAME        => 'dyalog-xml',
		   DESC        => 'A DyALog module to access LibXML',
		   TYPE        => $autotools,
		   FTP         => "$atollftp/TAG/dyalog-xml-1.0.3.tar.gz",
		   SVN         => "$atollsvn/dyalog/dyalog-xml/trunk",
		   PROJECT     => 'dyalog',
		   DEPEND      => { DyALog  => 1,
				    libxml2 => 1,
				    },
		   VERSION     => ["1.0.3", "dyalog-config", "--pkg=dyalog-xml --version"],
	       });

Packg::register( { NAME        => 'dyalog-sqlite',
		   DESC        => 'A DyALog module to access SQLITE3 databases',
		   TYPE        => $autotools,
		   FTP         => "$atollftp/TAG/dyalog-sqlite-1.0.0.tar.gz",
		   SVN         => "$atollsvn/dyalog/dyalog-sqlite/trunk",
		   PROJECT     => 'dyalog',
		   DEPEND      => { DyALog   => 1,
				    'sqlite' => 1,
				},
		   VERSION     => ["1.0.0", "dyalog-config", "--pkg=dyalog-sqlite --version"],
	       });


# project ALPAGE Linguistic Workbench
#################################################################

Packg::register( { NAME        => 'lexed',
		   DESC        => 'A lexicon manager',
		   TYPE        => $autotools,
		   FTP         => "$atollftp/TAG/lexed-4.7.tar.gz",
		   SVN         => "$atollsvn/lingwb/lexed/trunk",
		   PROJECT     => $lingwb,
		   VERSION     => ["4.7", "lexed-config", "--version"],
	       });

Packg::register( { NAME        => 'sxpipe',
		   DESC        => 'a pre-parsing chain for French including segmentation, spelling corrections, lexicon lookup, named entities detections, ... (use lingpipe, sxspell, text2dag)',
		   TYPE        => $autotools,
		   FTP         => 'https://gforge.inria.fr/frs/download.php/2338/sxpipe-1.2.1.tar.gz',
		   SVN         => "$atollsvn/lingwb/sxpipe/trunk",
		   PROJECT     => $lingwb,
		   DEPEND      => { 'syntax'          => 1,
				    'lefff'           => 1,
				},
		   CONFIGURE   => ["--with-lefffdir=$LENV{SHAREDIR}/lefff",
				   "--with-syntaxlib=$LENV{LIBDIR}/syntax/libsx.a",
				   "--with-syntaxlibdir=$LENV{LIBDIR}/syntax",
				   "--with-syntaxincl=$LENV{INCLUDEDIR}/syntax",
				   "--with-syntaxbin=$LENV{BINDIR}",
				   "--with-syntaxsrc=$LENV{SHAREDIR}/syntax",
				   "--with-alexinadir=$LENV{SHAREDIR}/alexina",
				   ],
		   VERSION     => ["1.2.1", "pkg-config", "--modversion sxpipe"],
	       });

Packg::register( { NAME        => 'Lingua-Features',
		   DESC        => 'Natural languages features',
		   TYPE        => $perl,
		   FTP         => "$atollftp/TAG/Lingua-Features-0.3.1.tar.gz",
		   SVN         => "$atollsvn/lingwb/Lingua-Features/trunk",
		   PROJECT     => $lingwb,
		   CPAN        => ['XML::Generator',
				   'Tie::IxHash',
				   'List::Compare'],
	       });

Packg::register( { NAME        => 'forest_utils',
		   DESC        => 'Perl conversion scripts for Shared Derivation Forests',
		   TYPE        => $perl,
		   FTP         => "$atollftp/TAG/forest_utils-0.09.tar.gz",
		   SVN         => "$atollsvn/lingwb/forest_utils/trunk",
		   PROJECT     => $lingwb,
		   CPAN        => [ 'AppConfig',
				    'XML::Parser',
				    'XML::Generator',
				    'Data::Grove',
				    'Data::Dumper',
				    'Parse::RecDescent',
				    ],
		   VERSION     => ["0.09", "Forest"],
	       });

Packg::register( { NAME        => 'parserd',
		   DESC        => 'A server of parsers with clients',
		   TYPE        => $autotools,
		   FTP         => "$atollftp/TAG/parserd-2.1.5.tar.gz",
		   SVN         => "$atollsvn/lingwb/parserd/trunk",
		   PROJECT     => $lingwb,
		   DEPEND      => { 'forest_utils' => 1 },
		   CPAN        => [ 'IO::Socket',
				    'AppConfig',
				    'File::Temp',
				    'Net::Server',
				    'Net::Telnet',
				    'CGI',
				    'IO::All',
				    'IPC::Run',
				    'IPC::Open2',
				    'IPC::Open3',
				    'Term::Report',
				    'Time::HiRes',
				    ],
		   POSTPROCESS => { update_conf    => 1 },
		   CONFIGURE   => ["--with-modperldir=$LENV{MODPERLDIR}",
				   "--with-cgidir=$LENV{CGIDIR}",
				   "--with-initdir=$LENV{INITDIR}",
				   "--localstatedir=$LENV{VAR}"],
		   VERSION     => ["2.1.5", "pkg-config", "--modversion parserd"], 
	       });


# project Metagrammar Toolkit
#################################################################

Packg::register( { NAME        => 'mgcomp',
		   DESC        => 'A Meta-Grammar compiler written in DyALog',
		   TYPE        => $autotools,
		   FTP         => "$atollftp/TAG/mgcomp-1.4.2.tar.gz",
		   SVN         => "$atollsvn/mgkit/mgcomp/trunk",
		   PROJECT     => $mgkit,
		   DEPEND      => { DyALog => 1 },
		   VERSION     => ["1.4.2", "pkg-config", "--modversion mgcomp"],
	       });

Packg::register( { NAME        => 'mgtools',
		   DESC        => 'An enviromnent to edit and view Meta-Grammars',
		   TYPE        => $autotools,
		   FTP         => "$atollftp/TAG/mgtools-2.2.0.tar.gz",
		   SVN         => "$atollsvn/mgkit/mgtools/trunk",
		   PROJECT     => $mgkit,
		   DEPEND      => { libxml2 => 1 },
		   CPAN        => [ 'XML::LibXML',
				    'AppConfig',
				    'Event'],
		   VERSION     => ["2.2.0", "pkg-config", "--modversion mgtools"],
	       });

Packg::register( { NAME        => 'frmg',
		   DESC        => 'A French Meta Grammar',
		   TYPE        => $autotools,
		   FTP         => "http://alpage.inria.fr/~cabrera/download/frmg-1.0.3.tar.gz",
		   SVN         => "$atollsvn/mgkit/frmg/trunk",
		   PROJECT     => $mgkit,
		   CONFIGURE   => ["--with-modperldir=$LENV{MODPERLDIR}",
				   "--sysconfdir=$LENV{ETC}"],
 		   DEPEND      => { DyALog            => 1,
				    'dyalog-xml'      => 1,
				    'dyalog-sqlite'   => 1,
				    mgcomp            => 1,
				    mgtools           => 1,
				    'lefff-frmg'      => 1,
				    'tag_utils'       => 1,
				    'parserd'         => 1,
				    'libxslt'         => 1,
				    'Lingua-Features' => 1,
 				},
		   CPAN        => [ 'AppConfig',
				    'Data::Compare',
				    'IPC::Open2',
				    'Parse::RecDescent',
				   ],
		   VERSION     => ["1.0.2", "pkg-config", "--modversion frmg"],
	       });

# Packg::register( { NAME        => 'biomg',
# 		   DESC        => 'A French Meta Grammar for botanical descriptions',
# 		   TYPE        => $autotools,
#                    FTP         => "http://atoll.inria.fr/~cabrera/download/biomg-0.0.1.tar.gz", # not 0.0.1, future version
# 		   SVN         => "$atollsvn/mgkit/biomg/trunk",
# 		   PROJECT     => $mgkit,
# 		   CONFIGURE   => ["--with-modperldir=$LENV{MODPERLDIR}",
# 				   "--sysconfdir=$LENV{ETC}"],
# 		   DEPEND      => { DyALog            => 1,
# 				    'dyalog-xml'      => 1,
# 				    'dyalog-sqlite'   => 1,
# 				    mgcomp            => 1,
# 				    mgtools           => 1,
# 				    'lefff-frmg'      => 1,
# 				    'tag_utils'       => 1,
# 				    'parserd'         => 1,
# 				    'forest_utils'    => 1,
# 				    'libxslt'         => 1,
# 				    'Lingua-Matcher'  => 1,
# 				    'Lingua-Features' => 1,
# 				},
# 		   CPAN        => [ 'AppConfig',
# 				    'Data::Compare',
# 				    'Data::Dumper',
# 				    'Encode',
# 				    'IPC::Open2',
# 				    ],
# 		   VERSION     => ["0.0.1", "pkg-config", "--modversion biomg"], 
# 	       });

Packg::register( { NAME        => 'tag_utils',
		   DESC        => 'Perl conversion scripts for Tree Adjoining Grammars',
		   TYPE        => $perl,
		   FTP         => "$atollftp/TAG/tag_utils-1.11.tar.gz",
		   SVN         => "$atollsvn/mgkit/tag_utils/trunk",
		   PROJECT     => $mgkit,
		   CPAN        => [ 'AppConfig',
				    'XML::Parser',
				    'DBI', 
				    'XML::Generator',
				    'Parse::Yapp',
				    'Data::Grove',
				    ],
		   VERSION     => ['1.11', 'TAG'],
		   });


# project Alexina
#################################################################

Packg::register( { NAME        => 'alexina-tools',
		   TYPE        => $autotools,
		   FTP         => "$atollftp/TAG/alexina-tools-1.0.1.tar.gz",
		   SVN         => "$atollsvn/alexina/alexina-tools/trunk",
		   PRIVATE     => 1,
		   PROJECT     => 'Alexina',
	           });

Packg::register( { NAME        => 'lefff',
		   DESC        => 'A French Morphological and Syntactic Lexicon',
		   TYPE        => $autotools,		   		   
		   FTP         => "https://gforge.inria.fr/frs/download.php/3782/lefff-2.5.5.tar.gz",
		   SVN         => "$atollsvn/alexina/lefff/trunk",
		   PRIVATE     => 1,
		   CONFIGURE   => ["--with-alexinatoolsdir=$LENV{SHAREDIR}/alexina-tools"],
		   PROJECT     => 'Alexina',
		   DEPEND      => { 'alexina-tools' => 1,
				    lexed           => 1,
				    },
	           });

Packg::register( { NAME        => 'lefff-frmg',
		   DESC        => 'Adaptation of Lefff for FRMG',
		   TYPE        =>  $autotools,
		   FTP         => "$atollftp/TAG/lefff-frmg-0.2.tar.gz",
		   SVN         => "$atollsvn/alexina/lefff-frmg/trunk",
		   PRIVATE     => 1,
		   CONFIGURE   => ["--with-lefffdir=$LENV{SHAREDIR}/lefff"],
		   PROJECT     => 'Alexina',
		   DEPEND      => { lexed => 1,
				    lefff => 1,
				    },
		   VERSION     => ["0.2", "pkg-config", "--modversion lefff-frmg"],
	           });


# SYNTAX
#################################################################

Packg::register( { NAME        => 'syntax',
		   DESC        => "To compile tools that rely on SYNTAX",
 		   TYPE        => $autotools,
		   FTP         => "https://gforge.inria.fr/frs/download.php/2337/syntax-core-6.0b2.tar.gz",
		   SVN         => "$atollsvn/syntax/trunk",
		   PROJECT     => 'syntax',
		   INSTALLED   => ["lib/syntax/libsx.a",
				   "include/syntax",
				   "share/syntax"],
 	       });


# non local dependencies
#################################################################

Packg::register( { NAME        => 'gc6.6',
		   DESC        => "Boehm\'s Garbage Collector",
		   TYPE        => $autotools,
		   NONLOCALDEP => 1,
		   FTP         => "http://www.hpl.hp.com/personal/Hans_Boehm/gc/gc_source/gc6.6.tar.gz",
		   INSTALLED   => ["include/gc/gc.h",
				   "lib/libgc.la",],

	       });

Packg::register( { NAME        => 'sqlite',
		   DESC        => 'C library that implements an SQL database engine',
		   NONLOCALDEP => 1,
		   TYPE        => $autotools,
		   FTP         => 'http://alpage.inria.fr/~cabrera/download/sqlite-3.3.10.tar.gz',
		   CONFIGURE   => ['--disable-tcl'],
 		   INSTALLED   => ["include/sqlite3.h",
				   "lib/libgc.la",],
		   #VERSION     => ["3.3.4", "sqlite3", "-version"], 
	       });

Packg::register( { NAME        => 'libxml2',
		   DESC        => 'XML C parser and toolkit',
		   NONLOCALDEP => 1,
		   TYPE        => $autotools,
		   FTP         => 'ftp://fr.rpmfind.net/pub/libxml/libxml2-2.6.26.tar.gz'  ,
		   INSTALLED   => ["include/libxml2/libxml/xmlversion.h",
				   "lib/libxml2.la",],
		   #VERSION     => ["2.6.23", "xml2-config", "--version"],
	       });

Packg::register( { NAME        => 'libxslt',
		   DESC        => 'XSLT C library',
		   NONLOCALDEP => 1,
		   TYPE        => $autotools,
		   FTP         => 'ftp://fr.rpmfind.net/pub/libxml/libxslt-1.1.17.tar.gz',
		   INSTALLED   => ["include/libexslt/exslt.h"],
		   VERSION     => ["1.1.9", "xslt-config", "--version"],
	       });

Packg::register( { NAME        => 'graphviz',
		   DESC        => 'Graph Visualization',
		   NONLOCALDEP => 1,
		   TYPE        => $autotools,
		   FTP         => 'http://www.graphviz.org/pub/graphviz/ARCHIVE/graphviz-2.8.tar.gz',
		   INSTALLED   => ["bin/dot"],
		   #VERSION     => ['2.8', 'dot', '-V'],
	       });

# INSTALLATOLL dependencies (if script is run by developper)
#################################################################

Packg::register( { NAME        => 'pkg-config',
		   ALPAGEDEP    => 1,
 		   NONLOCALDEP => 1,
		   TYPE        => '',
 		   VERSION     => ["0.15", "pkg-config", "--version"],
 	       });

Packg::register( { NAME        => 'autoconf',
		   ALPAGEDEP    => 1,
 		   NONLOCALDEP => 1,
		   TYPE        => '',
 		   VERSION     => ["2.59", "autoconf", "--version"],
 	       });

Packg::register( { NAME        => 'libtool',
		   ALPAGEDEP    => 1,
 		   NONLOCALDEP => 1,
		   TYPE        => '',
 		   VERSION     => ["1.5", "libtool", "--version"],
 	       });

Packg::register( { NAME        => 'automake',
		   ALPAGEDEP    => 1,
 		   NONLOCALDEP => 1,
		   TYPE        => '',
 		   VERSION     => ["1.9", "automake", "--version"],
 	       });


#################################################################

my @packages = @{$config->package};
@packages = sort keys(%Packg::package_list) unless (@packages);

# if machine is 64b and DyALog is installed, activate 32b emulation
# TODO
# if (!$config->linux32) {
#     my $bit = `uname -m`;
#     if ($bit =~ m/64/) {
# 	foreach my $pkg (@packages) {
# 	    if ($pkg eq "DyALog") {
# 		$config->linux32(1);
# 		return;
# 	    }
# 	    else {
# 		foreach
# 	    }
# 	}
#     }
# }

if ($config->info) {
    foreach my $package (@packages) {
	my $pkg = $Packg::package_list{$package};
	if (exists $pkg->{DESC}) {
	    print $pkg->{NAME}, "\t\t", $pkg->{DESC}, "\n";
	}
    }
    exit;
}

if ($config->makedist) {
    foreach my $package (@packages) {
	my $pkg = $Packg::package_list{$package};
	$pkg->makedist;
    }
    exit;
}


# print dependencies
if ($config->dependencies) {
    print join(" ", cpan_dep(@packages)), "\n\n";
    print_nonlocaldep(@packages);
    exit;
}

sub print_nonlocaldep {

    foreach my $package (@_) {
	my $pkg = $Packg::package_list{$package};
	
	next if ($pkg->{DEPCHECK});
	next if ($pkg->{ALPAGEDEP} && !$config->gforge_user);

	print_nonlocaldep( keys %{$pkg->{DEPEND}} ) if $pkg->{DEPEND};

	if ($pkg->{NONLOCALDEP} && !$pkg->uptodate()) {
	    my $dep = $pkg->{NAME};
	    $dep .= ' '.${$pkg->{VERSION}}[0] if $pkg->{VERSION};
	    print $dep, "\n";
	}
	$pkg->{DEPCHECK} = 1;
    }

}

# print versions of ALPAGE packages
if ($config->version) {

    foreach my $package (@packages) {
	if(!exists $Packg::package_list{$package}) {
	    verbose "Package '$package' does not exist.";
	    die;
	}
	else {
	    my $pkg = $Packg::package_list{$package};
	    next if ($pkg->{NONLOCALDEP}); # only print versions of ALPAGE pkg
	    if (exists $pkg->{VERSION}) {
		my $version = $pkg->version();
		verbose $pkg->{NAME}, " $version" if ($version) ;
	    }
	}
    }
    exit;
}

if ($config->project) {

    foreach my $pkg_name (@packages) {
	my $pkg = $Packg::package_list{$pkg_name};
	print $pkg->{NAME}, " => ", $pkg->{PROJECT}, "\n"
	    if (!$pkg->{NONLOCALDEP} && $pkg->{PROJECT});
    }
    exit;
}

############################################
# Purpose    : Check ALPAGE dependencies of this script

sub check_alpage_dep {
    my @packages = values (%Packg::package_list);
    my $message = '';
    foreach my $pkg (@packages) {
	if ($pkg->{ALPAGEDEP}) {
	    $message .= "$pkg->{NAME} @{$pkg->{VERSION}}[0]\n" if (!$pkg->uptodate) ;
	}
    }
    if ($message) {
	print "In order to run $0 you need:\n", $message;
	exit;
    }

    if ($Packg::package_list{autoconf}->version < 2.60) {
	my $lexed = $Packg::package_list{lexed};
	$lexed->{REVISION} = '1076' unless $lexed->{REVISION};
    }
}

############################################
# Purpose    : Run installation

if ($config->run) {
    check_alpage_dep() if ($config->gforge_user);
    emit_setenv_script();
    update();

    if ($config->rev) {
	chdir $src;
	open(FILE, ">versions.log") or die "can't open file: $!";
	printf(FILE "@Packg::version");
	close(FILE);
    }

    #verbose("\n**** Please type 'source $setenv_script' before using the packages");
    system("source $setenv_script");
    system("$parserd_setenv_script restart")
    #verbose("**** To run the server of parsers, type '$parserd_setenv_script start'.")
	if (grep { $_ eq 'parserd' } @packages);
}
close(LOG);


############################################
# Purpose    : Install or update packages

sub update {

    foreach my $package (@packages) {
	if(!exists $Packg::package_list{$package}) {
	    verbose "\nPackage '$package' does not exist...\n";
	    die;
	}
	else {
	    $Packg::package_list{$package}->update()
		if ($Packg::package_list{$package}->{TYPE});
	}
    }

}


############################################
# Purpose    : Get list of CPAN dependencies

sub cpan_dep {

    #my @modules = qw/IO::Tty/; # which package required this dep ??
    my @mod_list;

    foreach my $package (@_) {

	my $pkg = $Packg::package_list{$package};
	next if($pkg->{CPANCHECK});

	push (@mod_list, cpan_dep( keys %{$pkg->{DEPEND}} )) if ( $pkg->{DEPEND} );
	push (@mod_list, @{$pkg->{CPAN}}) if ( $pkg->{CPAN} );

	$pkg->{CPANCHECK} = 1;
    }

    # remove duplicate entries
    my %new_list = ();
    return grep {!$new_list{$_}++} sort @mod_list;
}


############################################
# Purpose    : Initialize environment variables

sub initenv {

    %LENV = (
	     PREFIX          => "$prefix",
	     BINDIR          => "$prefix/bin",
	     SBINDIR         => "$prefix/sbin",
	     LIBDIR          => "$prefix/lib",
	     DYALOGLIBDIR    => "$prefix/lib/DyALog",
	     INCLUDEDIR      => "$prefix/include",
	     SHAREDIR        => "$prefix/share",
	     MANDIR          => "$prefix/man",
	     ACLOCAL         => "$prefix/share/aclocal",
	     ETC             => "$prefix/etc",
	     VAR             => "$vardir/var",
	     PERL5LIB        => "$prefix/lib/perl5:$prefix/lib/perl:$prefix/lib/perl5/site_perl:$prefix/lib/perl5/site_perl/$perlversion:$prefix/lib/perl/$perlversion/auto:$prefix/share/automake-1.9:$prefix/share/perl/$perlversion:$prefix/lib/perl/$perlversion:$prefix/lib/perl/5.8:$prefix/lib/share/perl/$perlversion:$prefix/share/perl/5.8:/usr/lib/perl5/vendor_perl/5.8.8",
	     CGIDIR          => "$vardir/var/www/cgi-bin",
	     MODPERLDIR      => "$vardir/var/www/perl",
	     INITDIR         => "$prefix/etc/init.d",
	     PKG_CONFIG_PATH => "$prefix/lib/pkgconfig",
	     sx              => "$src/syntax/linux-i386",
	     sxV             => "6.0",
	     sxSRC           => "$src/syntax/"
	     );

    #$ENV{LC_ALL}         = "fr_FR";
    $ENV{LD_RUN_PATH}     = "$LENV{LIBDIR}:$LENV{DYALOGLIBDIR}";
    $ENV{LD_LIBRARY_PATH} = "$LENV{LIBDIR}:$LENV{DYALOGLIBDIR}";
    $ENV{LDFLAGS}         = "-L$LENV{LIBDIR}";
    $ENV{PERL5LIB}        = "$LENV{PERL5LIB}:$ENV{PERL5LIB}";
    $ENV{PATH}            = "$LENV{sx}/bin:$LENV{BINDIR}:$ENV{PATH}:/usr/local/bin";
    $ENV{CPPFLAGS}        = "-I$LENV{INCLUDEDIR}";
    $ENV{PKG_CONFIG_PATH} = $LENV{PKG_CONFIG_PATH};
    $ENV{sx}              = $LENV{sx};
    $ENV{sxV}             = $LENV{sxV};
    $ENV{ACLOCAL}         = "aclocal -I $LENV{ACLOCAL}"; # because option -I is not passed through autoreconf
    $ENV{FTP_PASSIVE}     = 1; # for cpan

    #note: $prefix empty at compile time, cannot use it here
    use lib "$LENV{LIBDIR}/perl5/site_perl"; # to be able to check modules versions
    use lib "$LENV{LIBDIR}/perl/$perlversion/auto";
    use lib "$LENV{LIBDIR}/perl5"; # to check modules locally installed with cpan
    use lib "$ENV{PERL5LIB}"; # to check installed modules

}

############################################
# Purpose    : Emit script to initialize environment variables
# Comments   : The user will need to type
#                 source <prefix>/sbin/setenv.sh
#              or add the file to his .bash_profile

sub emit_setenv_script {
  ## Some of the following paths should not be necessary thanks to libtool
  ## but we just add them in case of !
  my $libpath = join(':',
		       qw{$ALPAGE_PREFIX/lib/
			  $ALPAGE_PREFIX/lib/DyALog/
			  $ALPAGE_PREFIX/lib/DyALog/dyalog-xml
			  $ALPAGE_PREFIX/lib/DyALog/dyalog-sqlite
			}
		      );
  my $version = sprintf("%vd",$^V);


  ## setenv script
  $setenv_script = $LENV{SBINDIR}."/setenv.sh";
  open(SCRIPT,">$setenv_script") || die "can't open $setenv_script: $!";
  print SCRIPT <<EOF;
## Source this file or add it to your .bash_profile

## Edit following variable
export ALPAGE_PREFIX="$LENV{PREFIX}"

## Following variables should be ok
#export LC_ALL=fr_FR
export LD_RUN_PATH="$libpath:\$LD_RUN_PATH"
export LD_LIBRARY_PATH="$libpath:\$LD_LIBRARY_PATH"
export PATH="\$ALPAGE_PREFIX/bin/:\$ALPAGE_PREFIX/share/frmg/:\$ALPAGE_PREFIX/share/biomg/:\$ALPAGE_PREFIX/sbin/:\$PATH"
export LDFLAGS="-L\${ALPAGE_PREFIX}/lib"
export PERL5LIB="\${ALPAGE_PREFIX}/lib/perl5/site_perl:\${ALPAGE_PREFIX}/lib/perl5:\${ALPAGE_PREFIX}/lib/perl5/site_perl/$version:\${ALPAGE_PREFIX}/lib/perl/$version/auto:\${ALPAGE_PREFIX}/share/automake-1.9:\${ALPAGE_PREFIX}/share/perl/$version:\${ALPAGE_PREFIX}/lib/perl/$version:\${ALPAGE_PREFIX}/lib/perl:\${ALPAGE_PREFIX}/lib/perl/5.8:\${ALPAGE_PREFIX}/share/perl/5.8:\$PERL5LIB:/usr/lib/perl5/vendor_perl/5.8.8"
export PKG_CONFIG_PATH="\${ALPAGE_PREFIX}/lib/pkgconfig:\$PKG_CONFIG_PATH"

EOF
  close(SCRIPT);

  chmod 0755, $setenv_script;
  #verbose("\n**** Please source $setenv_script before using packages");


  ## parserd_setenv script
  $parserd_setenv_script = $LENV{SBINDIR}."/parserd_setenv";
  open(SCRIPT,">$parserd_setenv_script") || die "can't open $parserd_setenv_script: $!";
  print SCRIPT <<EOF;
## Source this file or add it to your .bash_profile

## Edit following variable
export ALPAGE_PREFIX="$LENV{PREFIX}"

## Following variables should be ok
#export LC_ALL=fr_FR
export LD_RUN_PATH="$libpath:\$LD_RUN_PATH"
export LD_LIBRARY_PATH="$libpath:\$LD_LIBRARY_PATH"
export PATH="\$ALPAGE_PREFIX/bin/:\$ALPAGE_PREFIX/sbin/:\$PATH"
export PERL5LIB="\${ALPAGE_PREFIX}/lib/perl5/site_perl:\${ALPAGE_PREFIX}/lib/perl5:\${ALPAGE_PREFIX}/lib/perl5/site_perl/$version:\${ALPAGE_PREFIX}/lib/perl/$version/auto:\${ALPAGE_PREFIX}/share/automake-1.9:\${ALPAGE_PREFIX}/share/perl/$version:\${ALPAGE_PREFIX}/lib/perl/$version:\${ALPAGE_PREFIX}/lib/perl:\${ALPAGE_PREFIX}/lib/perl/5.8:\${ALPAGE_PREFIX}/share/perl/5.8:\$PERL5LIB:/usr/lib/perl5/vendor_perl/5.8.8"

exec \$ALPAGE_PREFIX/sbin/parserd_service \$*
EOF
  close(SCRIPT);

  chmod 0755, $parserd_setenv_script;
  #verbose("**** Please run $parserd_setenv_script to start/stop parserd server");
}

=head1 NAME

installatoll.pl -- Local install of ALPAGE (formerly ATOLL) French linguistic processing chain

=head1 SYNOPSIS 	 
  	 
./installatoll.pl [options] 	 
  	 
where the options are 	 
 	 
=over 4 	 
  	 
=item --config=F<file>                  (to read a specific config file [default F<installatoll.conf>]) 	 

=item --revconfig=F<file>               (to download specific svn revisions [default F<rev.conf>])

=item --prefix=F<path>                  [default F</home/toto/exportbuild>] 	 
  	 
=item --vardir=F<path>                  [default F</tmp/toto/exportbuild>] 	 
  	 
=item --port|p=<number>              [default 9043] 	 
  	 
=item --user|u=<user>

=item --group|g=<group>
  	 
=item --gforge_user|gf=<gforge_user> (to download sources from the svn repository on Gforge) 	 
  	 
=item --package|pkg=<package>        (to download a specific package)

=item --skip_pkg=<package>

=item --skip_dep=<package>            (to skip installation of all dependencies)
  	 
=item --linux32                      (to force 32bit emulation mode on 64bit machines) 	 
  	 
=item --force|f
  	 
=item --dependencies|dep             (to list the dependencies needed to run this script) 	 
  	 
=item --version|v                    (to print versions of Alpage packages) 

=item --project                      (to print Gforge project name of each package)

=item --info                         (information about packages installed by installatoll)

=item --log=<log file>               [default installatoll.log]

=item --test                         run packages tests [default no]
  	 
=back

=head1 DESCRIPTION

B<installatoll.pl> is a Perl script to help users install locally
ALPAGE software. It can also be used to install a full linguistic
processing chain for French based on:

=over 4

=item DyALog, a parser compiler and logic programming enviromnent

=item FRMG, a French Meta Grammar

=item Lefff, a French Morphological and Syntactic Lexicon

=item SxPipe, a pre-parsing chain for French including segmentation, spelling corrections, lexicon lookup, named entities detections, ...

=back

A server of parser is also installed.

A demo of the linguistic processing chain is available on this page: 
L<http://alpage.inria.fr/parserdemo>

B<installatoll.pl> either installs the whole processing chain or only
the packages specified by the user. The packages are retrieved by FTP
(default) or from the Subversion repository.

The B<documentation> for each package installed by B<installatoll.pl>
should be under the directory where the sources of the packages
were downloaded. The default location is
I<${HOME}/exportbuild/src/{PACKAGE}>.

You can also find various papers and some documentation on these
pages:

=over 4

=item DyALog                      : L<http://alpage.inria.fr/dyalog.en.html>

=item ALPAGE Linguistic Workbench  : L<http://lingwb.gforge.inria.fr>

=item Metagrammar Toolkit         : L<http://mgkit.gforge.inria.fr>

=item Lefff                       : L<http://alexina.gforge.inria.fr>

=back

=head1 INSTALLING THE FRENCH PROCESSING CHAIN

=over 4

=item See the INSTALL file for the installation procedure. INSTALL is provided with B<installatoll.pl> release. It can also be accessed any time at L<https://gforge.inria.fr/plugins/scmsvn/viewcvs.php/installatoll/trunk/INSTALL?root=lingwb&view=markup>.

=item Don't forget to check the FAQ : L<http://alpage.inria.fr/installatoll_faq.en.html>.

=back

=head1 USING THE FRENCH PROCESSING CHAIN

=head2 USING THE PARSERS THROUGH THE SERVER OF PARSERS

The server of parser may be started and stopped by the following commands

    ${PREFIX}/sbin/parserd_setenv start
    ${PREFIX}/sbin/parserd_setenv stop

The server runs on port specified by the option B<--port> [default 9043] under
identity specified by B<--user> and B<--group>.

=head3 * callparser

The server of parser can be indirectly accessed 
through the command B<callparser>.

To use command callparser,
you need to set up environment variables typing: 

    source ${PREFIX}/sbin/setenv.sh

To display dependencies:

    echo "il mange une pomme" | \
    callparser -in - -p <port> -time -stats -d dep

To run a sentence file (one sentence per line):

    cat <file> | callparser -in - -p <port> -time -stats [options]

You can try:

    cat sentences.txt | callparser -in - -p 9043 -time -stats

B<callparser> has many options to visualize the parsing output (as
HTML, XML, raw, dot, ...). To find more information regarding B<callparser> use, 
please type:

    perldoc callparser
    
=head3 * telnet

The server may also be directly accessed with B<telnet>:

    telnet localhost 9043

Then press the return key twice an try:

    frmgtel il mange une pommme

To set a forest format and then print it:

    set forest dependency
    last forest

Please type help to list commands and options.

=head2 USING THE PARSERS DIRECTLY

Of course, the parsers may be directly used. To do that, you first need
to setup environment variables:

    source ${PREFIX}/sbin/setenv.sh

note: the content of F<setenv.sh> may also be directly included in your F<.bash_profile>

Then, something like the following line should work ! (Of course, do
not type '\', this is a single command line)

    echo "il mange une pomme" | easy.pl |\
    tig_parser ${PREFIX}/share/frmg/small_header.tag - -forest
    
To get a graphical view of the forest, try something like

    echo "il mange une pomme" | easy.pl |\
    tig_parser ${PREFIX}/share/frmg/small_header.tag - -forest |\
    forest_convert.pl -f lp -t dep | dot -Tgif | display
    
The graph displayed will contain such information as:

    mange - manger:v:117 (2)

where "manger" is the lemma, "v" the grammatical category (verb), 
"117" the tree number and (2), the number of derivations from this node.

B<forest_convert.pl> (package B<forest_utils>) has many conversion
options, most of them similar to the ones used by B<callparser>. For
information regarding forest_convert.pl, please type:

    perldoc forest_convert.pl

=head2 USING THE PARSERS THROUGH A WEB INTERFACE

You can either use our online demo here: 
L<http://alpage.inria.fr/parserdemo>

or you can run that interface on your machine. This requires the
installation of an apache server. Please follow the instructions
provided in B<parserd> package to install the interface. F<README> and
F<INSTALL> files of parserd should be under I<${PREFIX}/src/parserd/>.

=head2 WHEN A SENTENCE IS NOT SUCCESSFULLY PARSED...

There are many reasons why a sentence cannot be parserd. It can be a
problem with the grammar (FRMG), with the lexicon (Lefff), and so
on... If you notice any problem, please notify us. The more feedback we
have, the more we can improve the processing chain. Still there are
some commands you can try to trace or temporarily solve the problem.

Check the output of easy.pl:

    echo "Je teste une phrase" | easy.pl

If you get something like "lex => '_uw'" in the output, it means that
there is an unknown word. Or maybe a word is tagged with the wrong
category, for instance "adj" instead of "np". This means that the word
is not in the lexicon. You can fix this by editing F<missing.lex>
which you can find under I<${PREFIX}/share/frmg/>.

To directly check an entry in the lexicon, type:

    echo "Luc" | lexed -d ${PREFIX}/lib/lefff-frmg -p dico.xlfg consult

You will get somehting like:

    np      [pred='Luc_____1<Suj:(sn)>',cat=np,@loc,@ms]    Luc_____1

You can find information about lexed usage typing:

    lexed -usage

=head1 A LINGUISTIC TOOLKIT TO DESIGN GRAMMARS

B<installatoll.pl> installs a linguistic development toolkit to build and
compile your own grammars. The best starting point is to look at the
package B<frmg> (under I<${PREFIX}/src/>).

A meta-grammar (MG) in format SMG [file extension I<.smg>] may be
edited under Emacs with a syntactic mode provided by B<mg.el> (in
package B<mgtools>). The class hierarchy may be visualized with
B<mgviewer> (packgage B<mgtools>), provided B<uDraw> (formerly B<daVinci>) is
installed.

The meta-grammar may then be converted in XML format with B<smg2xml>
(package B<mgtools>) and then to a DyALog format through an XSLT
transformation (see Makefile.am in B<frmg>), file extensions I<.xml>
and I<.mg.pl>.

The meta-grammar in DyALog format can be compiled with B<mgcomp>
(package B<mgcomp>) into a XML representation [TAGML] of a Tree
Adjoining Grammar [TAG], file extension I<.tag.xml>.

The grammar may then converted in a DyALog format [file extension
I<.tag>] with B<tag_converter> (package B<tag_utils>). The grammar in
DyALog format may then be compiled into a TAG or hybrid TIG/TAG parser
with B<dyacc> (package B<DyALog>).

=head1 PACKAGES

To see which packages are installed by B<installatoll.pl>, please type:

    ./installatoll --info

=head1 DOWNLOAD INSTALLATOLL

The latest release can be retrieved from the following WEB page: 
L<https://gforge.inria.fr/frs/?group_id=481>

A snapshot is created every day:

L<http://alpage.inria.fr/~cabrera/download/installatoll/installatoll-snapshot-last.tar.gz>

The latest version of this script can also be retrieved from the 
Subversion repository typing:

    svn co svn://scm.gforge.inria.fr/svn/lingwb/installatoll/trunk installatoll

If you have already made a checkout of installatoll, in order to
check that you have the latest version, cd in its directory and type

    svn update

=head1 AUTHORS

=over 4

=item Eric de la Clergerie, <Eric.De_La_Clergerie@inria.fr>

=item Isabelle Cabrera, <Isabelle.Cabrera@inria.fr>

=back

=head1 SEE ALSO

=over 4

=item INSTALL file: L<https://gforge.inria.fr/plugins/scmsvn/viewcvs.php/installatoll/trunk/INSTALL?root=lingwb&view=markup>

=item FAQ : L<http://alpage.inria.fr/installatoll_faq.en.html>

=item Demo : L<http://alpage.inria.fr/parserdemo>

=item Inria GForge project page : ALPAGE Linguistic workbench L<http://lingwb.gforge.inria.fr>

=item Subversion repository : L<https://gforge.inria.fr/plugins/scmsvn/viewcvs.php/installatoll/?root=lingwb>

=item Alpage Home page : L<http://alpage.inria.fr>

=back


=cut
